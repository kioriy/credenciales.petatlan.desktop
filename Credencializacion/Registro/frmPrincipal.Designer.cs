﻿namespace Registro
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.label5 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.gbDireccion = new System.Windows.Forms.GroupBox();
            this.dtpExpiracion = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpExpedicion = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbTipoLicencia = new System.Windows.Forms.ComboBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gbDatosGenerales = new System.Windows.Forms.GroupBox();
            this.txtAlergias = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.rbFemenino = new System.Windows.Forms.RadioButton();
            this.rbMasculino = new System.Windows.Forms.RadioButton();
            this.lblsexo = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.cmbLote = new System.Windows.Forms.ComboBox();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblSangre = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.tsPrincipal = new System.Windows.Forms.ToolStrip();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.tsbLimpiar = new System.Windows.Forms.ToolStripButton();
            this.tsbEditarFirma = new System.Windows.Forms.ToolStripButton();
            this.pbImagenFondo = new System.Windows.Forms.PictureBox();
            this.pbCodeBar = new System.Windows.Forms.PictureBox();
            this.pbImagen = new System.Windows.Forms.PictureBox();
            this.pbCaptura = new System.Windows.Forms.PictureBox();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.ofdAbrirImagen = new System.Windows.Forms.OpenFileDialog();
            this.lDireccion = new System.Windows.Forms.Label();
            this.lLote = new System.Windows.Forms.Label();
            this.lCurp = new System.Windows.Forms.Label();
            this.lSexo = new System.Windows.Forms.Label();
            this.lNombre = new System.Windows.Forms.Label();
            this.pbFondo = new System.Windows.Forms.PictureBox();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.lblLicencia = new System.Windows.Forms.Label();
            this.lblAlergias = new System.Windows.Forms.Label();
            this.lblExpedicion = new System.Windows.Forms.Label();
            this.lblExpiracion = new System.Windows.Forms.Label();
            this.gbEmergencia = new System.Windows.Forms.GroupBox();
            this.txtTelEmergencia = new System.Windows.Forms.TextBox();
            this.lblTelEmergencia = new System.Windows.Forms.Label();
            this.txtEmergencia = new System.Windows.Forms.TextBox();
            this.lblAvisar = new System.Windows.Forms.Label();
            this.lblEmergencia = new System.Windows.Forms.Label();
            this.lblTelEmer = new System.Windows.Forms.Label();
            //this.sigPlusNET1 = new Topaz.SigPlusNET();
            this.gbFirmas = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnDetener = new System.Windows.Forms.Button();
            this.bntIniciar = new System.Windows.Forms.Button();
            this.pbFirma1 = new System.Windows.Forms.PictureBox();
            this.pbFirma2 = new System.Windows.Forms.PictureBox();
            this.gbDireccion.SuspendLayout();
            this.gbDatosGenerales.SuspendLayout();
            this.tsPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenFondo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCodeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCaptura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).BeginInit();
            this.gbEmergencia.SuspendLayout();
            this.gbFirmas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFirma1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFirma2)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(500, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Apellido(s):";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(253, 22);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(58, 13);
            this.lblNombre.TabIndex = 5;
            this.lblNombre.Tag = "Nosidf";
            this.lblNombre.Text = "Nombre(s):";
            // 
            // gbDireccion
            // 
            this.gbDireccion.BackColor = System.Drawing.SystemColors.Menu;
            this.gbDireccion.Controls.Add(this.dtpExpiracion);
            this.gbDireccion.Controls.Add(this.label6);
            this.gbDireccion.Controls.Add(this.dtpExpedicion);
            this.gbDireccion.Controls.Add(this.label4);
            this.gbDireccion.Controls.Add(this.label3);
            this.gbDireccion.Controls.Add(this.cmbTipoLicencia);
            this.gbDireccion.Location = new System.Drawing.Point(12, 214);
            this.gbDireccion.Name = "gbDireccion";
            this.gbDireccion.Size = new System.Drawing.Size(574, 74);
            this.gbDireccion.TabIndex = 7;
            this.gbDireccion.TabStop = false;
            this.gbDireccion.Text = "Datos Licencia";
            // 
            // dtpExpiracion
            // 
            this.dtpExpiracion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpExpiracion.Location = new System.Drawing.Point(453, 24);
            this.dtpExpiracion.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpExpiracion.Name = "dtpExpiracion";
            this.dtpExpiracion.Size = new System.Drawing.Size(105, 20);
            this.dtpExpiracion.TabIndex = 22;
            this.dtpExpiracion.Value = new System.DateTime(2015, 11, 9, 0, 0, 0, 0);
            this.dtpExpiracion.ValueChanged += new System.EventHandler(this.dtpExpiracion_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(343, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Fecha de Expiración:";
            // 
            // dtpExpedicion
            // 
            this.dtpExpedicion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpExpedicion.Location = new System.Drawing.Point(225, 23);
            this.dtpExpedicion.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpExpedicion.Name = "dtpExpedicion";
            this.dtpExpedicion.Size = new System.Drawing.Size(103, 20);
            this.dtpExpedicion.TabIndex = 21;
            this.dtpExpedicion.Value = new System.DateTime(2015, 11, 9, 13, 34, 54, 0);
            this.dtpExpedicion.ValueChanged += new System.EventHandler(this.dtpExpedicion_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(108, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Fecha de Expedición:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tipo:";
            // 
            // cmbTipoLicencia
            // 
            this.cmbTipoLicencia.FormattingEnabled = true;
            this.cmbTipoLicencia.Location = new System.Drawing.Point(40, 23);
            this.cmbTipoLicencia.Name = "cmbTipoLicencia";
            this.cmbTipoLicencia.Size = new System.Drawing.Size(54, 21);
            this.cmbTipoLicencia.TabIndex = 5;
            this.cmbTipoLicencia.SelectedIndexChanged += new System.EventHandler(this.cmbTipoLicencia_SelectedIndexChanged_1);
            this.cmbTipoLicencia.Leave += new System.EventHandler(this.cmbCoto_Leave);
            // 
            // txtNumero
            // 
            this.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumero.Location = new System.Drawing.Point(399, 93);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(56, 20);
            this.txtNumero.TabIndex = 8;
            this.txtNumero.TextChanged += new System.EventHandler(this.txtNumero_TextChanged);
            this.txtNumero.Leave += new System.EventHandler(this.txtNumero_Leave);
            // 
            // txtCalle
            // 
            this.txtCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCalle.Location = new System.Drawing.Point(53, 93);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(286, 20);
            this.txtCalle.TabIndex = 7;
            this.txtCalle.TextChanged += new System.EventHandler(this.txtCalle_TextChanged);
            this.txtCalle.Leave += new System.EventHandler(this.txtCalle_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(350, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Número:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 5;
            this.label7.Tag = "Nosidf";
            this.label7.Text = "Calle:";
            // 
            // gbDatosGenerales
            // 
            this.gbDatosGenerales.BackColor = System.Drawing.SystemColors.MenuBar;
            this.gbDatosGenerales.Controls.Add(this.txtAlergias);
            this.gbDatosGenerales.Controls.Add(this.label9);
            this.gbDatosGenerales.Controls.Add(this.txtNumero);
            this.gbDatosGenerales.Controls.Add(this.label2);
            this.gbDatosGenerales.Controls.Add(this.rbFemenino);
            this.gbDatosGenerales.Controls.Add(this.txtCalle);
            this.gbDatosGenerales.Controls.Add(this.rbMasculino);
            this.gbDatosGenerales.Controls.Add(this.label7);
            this.gbDatosGenerales.Controls.Add(this.lblsexo);
            this.gbDatosGenerales.Controls.Add(this.txtNombre);
            this.gbDatosGenerales.Controls.Add(this.dtpFecha);
            this.gbDatosGenerales.Controls.Add(this.cmbLote);
            this.gbDatosGenerales.Controls.Add(this.txtCurp);
            this.gbDatosGenerales.Controls.Add(this.txtApellido);
            this.gbDatosGenerales.Controls.Add(this.label8);
            this.gbDatosGenerales.Controls.Add(this.lblSangre);
            this.gbDatosGenerales.Controls.Add(this.label1);
            this.gbDatosGenerales.Controls.Add(this.lblNombre);
            this.gbDatosGenerales.Controls.Add(this.label5);
            this.gbDatosGenerales.Location = new System.Drawing.Point(12, 78);
            this.gbDatosGenerales.Name = "gbDatosGenerales";
            this.gbDatosGenerales.Size = new System.Drawing.Size(853, 126);
            this.gbDatosGenerales.TabIndex = 6;
            this.gbDatosGenerales.TabStop = false;
            this.gbDatosGenerales.Text = "Datos Generales";
            // 
            // txtAlergias
            // 
            this.txtAlergias.Location = new System.Drawing.Point(543, 92);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.Size = new System.Drawing.Size(200, 20);
            this.txtAlergias.TabIndex = 21;
            this.txtAlergias.TextChanged += new System.EventHandler(this.txtAlergias_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(491, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Alergias: ";
            // 
            // rbFemenino
            // 
            this.rbFemenino.AutoSize = true;
            this.rbFemenino.Location = new System.Drawing.Point(149, 59);
            this.rbFemenino.Name = "rbFemenino";
            this.rbFemenino.Size = new System.Drawing.Size(71, 17);
            this.rbFemenino.TabIndex = 19;
            this.rbFemenino.TabStop = true;
            this.rbFemenino.Text = "Femenino";
            this.rbFemenino.UseVisualStyleBackColor = true;
            this.rbFemenino.CheckedChanged += new System.EventHandler(this.rbFemenino_CheckedChanged);
            // 
            // rbMasculino
            // 
            this.rbMasculino.AutoSize = true;
            this.rbMasculino.Location = new System.Drawing.Point(60, 59);
            this.rbMasculino.Name = "rbMasculino";
            this.rbMasculino.Size = new System.Drawing.Size(73, 17);
            this.rbMasculino.TabIndex = 18;
            this.rbMasculino.TabStop = true;
            this.rbMasculino.Text = "Masculino";
            this.rbMasculino.UseVisualStyleBackColor = true;
            this.rbMasculino.CheckedChanged += new System.EventHandler(this.rbMasculino_CheckedChanged);
            // 
            // lblsexo
            // 
            this.lblsexo.AutoSize = true;
            this.lblsexo.Location = new System.Drawing.Point(9, 61);
            this.lblsexo.Name = "lblsexo";
            this.lblsexo.Size = new System.Drawing.Size(34, 13);
            this.lblsexo.TabIndex = 17;
            this.lblsexo.Text = "Sexo:";
            // 
            // txtNombre
            // 
            this.txtNombre.AcceptsReturn = true;
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Location = new System.Drawing.Point(313, 19);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(177, 20);
            this.txtNombre.TabIndex = 0;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(125, 21);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(112, 20);
            this.dtpFecha.TabIndex = 16;
            this.dtpFecha.Value = new System.DateTime(2015, 4, 9, 0, 0, 0, 0);
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            // 
            // cmbLote
            // 
            this.cmbLote.FormattingEnabled = true;
            this.cmbLote.Location = new System.Drawing.Point(580, 59);
            this.cmbLote.Name = "cmbLote";
            this.cmbLote.Size = new System.Drawing.Size(64, 21);
            this.cmbLote.TabIndex = 6;
            this.cmbLote.SelectedIndexChanged += new System.EventHandler(this.cmbLote_SelectedIndexChanged);
            this.cmbLote.TextChanged += new System.EventHandler(this.cmbLote_TextChanged);
            this.cmbLote.Leave += new System.EventHandler(this.cmbLote_Leave);
            // 
            // txtCurp
            // 
            this.txtCurp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurp.Location = new System.Drawing.Point(277, 58);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(186, 20);
            this.txtCurp.TabIndex = 3;
            this.txtCurp.TextChanged += new System.EventHandler(this.txtCurp_TextChanged);
            this.txtCurp.Leave += new System.EventHandler(this.txtPuesto_Leave);
            // 
            // txtApellido
            // 
            this.txtApellido.AcceptsReturn = true;
            this.txtApellido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApellido.Location = new System.Drawing.Point(558, 19);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(190, 20);
            this.txtApellido.TabIndex = 1;
            this.txtApellido.TextChanged += new System.EventHandler(this.txtapellido_p_TextChanged);
            this.txtApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtapellido_p_KeyPress);
            this.txtApellido.Leave += new System.EventHandler(this.txtapellido_p_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Fecha de Nacimiento:";
            // 
            // lblSangre
            // 
            this.lblSangre.AutoSize = true;
            this.lblSangre.Location = new System.Drawing.Point(491, 63);
            this.lblSangre.Name = "lblSangre";
            this.lblSangre.Size = new System.Drawing.Size(83, 13);
            this.lblSangre.TabIndex = 14;
            this.lblSangre.Text = "Tipo de Sangre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(238, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Curp:";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.BackColor = System.Drawing.Color.Transparent;
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(244, 396);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(51, 12);
            this.lblFecha.TabIndex = 13;
            this.lblFecha.Text = "00/00/0000";
            // 
            // tsPrincipal
            // 
            this.tsPrincipal.BackColor = System.Drawing.SystemColors.Highlight;
            this.tsPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbImprimir,
            this.tsbLimpiar,
            this.tsbEditarFirma});
            this.tsPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tsPrincipal.Name = "tsPrincipal";
            this.tsPrincipal.Size = new System.Drawing.Size(871, 70);
            this.tsPrincipal.TabIndex = 8;
            this.tsPrincipal.Text = "toolStrip1";
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbImprimir.Image = ((System.Drawing.Image)(resources.GetObject("tsbImprimir.Image")));
            this.tsbImprimir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(74, 67);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimir.Click += new System.EventHandler(this.tsbImprimir_Click);
            // 
            // tsbLimpiar
            // 
            this.tsbLimpiar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("tsbLimpiar.Image")));
            this.tsbLimpiar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLimpiar.Name = "tsbLimpiar";
            this.tsbLimpiar.Size = new System.Drawing.Size(87, 67);
            this.tsbLimpiar.Text = "Limpiar Datos";
            this.tsbLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbLimpiar.Click += new System.EventHandler(this.tsbLimpiar_Click);
            // 
            // tsbEditarFirma
            // 
            this.tsbEditarFirma.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbEditarFirma.Image = ((System.Drawing.Image)(resources.GetObject("tsbEditarFirma.Image")));
            this.tsbEditarFirma.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbEditarFirma.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditarFirma.Name = "tsbEditarFirma";
            this.tsbEditarFirma.Size = new System.Drawing.Size(77, 67);
            this.tsbEditarFirma.Text = "Editar Firma";
            this.tsbEditarFirma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbEditarFirma.Click += new System.EventHandler(this.tsbEditarFirma_Click);
            // 
            // pbImagenFondo
            // 
            this.pbImagenFondo.Image = ((System.Drawing.Image)(resources.GetObject("pbImagenFondo.Image")));
            this.pbImagenFondo.Location = new System.Drawing.Point(37, 296);
            this.pbImagenFondo.Name = "pbImagenFondo";
            this.pbImagenFondo.Size = new System.Drawing.Size(331, 189);
            this.pbImagenFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagenFondo.TabIndex = 17;
            this.pbImagenFondo.TabStop = false;
            // 
            // pbCodeBar
            // 
            this.pbCodeBar.BackColor = System.Drawing.Color.Transparent;
            this.pbCodeBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbCodeBar.Location = new System.Drawing.Point(905, 242);
            this.pbCodeBar.Name = "pbCodeBar";
            this.pbCodeBar.Size = new System.Drawing.Size(93, 30);
            this.pbCodeBar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbCodeBar.TabIndex = 27;
            this.pbCodeBar.TabStop = false;
            // 
            // pbImagen
            // 
            this.pbImagen.Location = new System.Drawing.Point(51, 363);
            this.pbImagen.Name = "pbImagen";
            this.pbImagen.Size = new System.Drawing.Size(82, 95);
            this.pbImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbImagen.TabIndex = 28;
            this.pbImagen.TabStop = false;
            this.pbImagen.Click += new System.EventHandler(this.pbImagen_Click);
            // 
            // pbCaptura
            // 
            this.pbCaptura.Location = new System.Drawing.Point(52, 363);
            this.pbCaptura.Name = "pbCaptura";
            this.pbCaptura.Size = new System.Drawing.Size(82, 95);
            this.pbCaptura.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbCaptura.TabIndex = 29;
            this.pbCaptura.TabStop = false;
            this.pbCaptura.Click += new System.EventHandler(this.pbCaptura_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // ofdAbrirImagen
            // 
            this.ofdAbrirImagen.FileName = "openFileDialog1";
            // 
            // lDireccion
            // 
            this.lDireccion.AutoSize = true;
            this.lDireccion.BackColor = System.Drawing.Color.Transparent;
            this.lDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDireccion.Location = new System.Drawing.Point(398, 314);
            this.lDireccion.Name = "lDireccion";
            this.lDireccion.Size = new System.Drawing.Size(44, 12);
            this.lDireccion.TabIndex = 24;
            this.lDireccion.Text = "Direccion";
            this.lDireccion.TextChanged += new System.EventHandler(this.lblDireccion_TextChanged);
            // 
            // lLote
            // 
            this.lLote.AutoSize = true;
            this.lLote.BackColor = System.Drawing.Color.Transparent;
            this.lLote.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lLote.Location = new System.Drawing.Point(604, 347);
            this.lLote.Name = "lLote";
            this.lLote.Size = new System.Drawing.Size(22, 12);
            this.lLote.TabIndex = 23;
            this.lLote.Text = "Tipo";
            // 
            // lCurp
            // 
            this.lCurp.AutoSize = true;
            this.lCurp.BackColor = System.Drawing.Color.Transparent;
            this.lCurp.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lCurp.Location = new System.Drawing.Point(398, 344);
            this.lCurp.Name = "lCurp";
            this.lCurp.Size = new System.Drawing.Size(25, 12);
            this.lCurp.TabIndex = 22;
            this.lCurp.Text = "Curp";
            // 
            // lSexo
            // 
            this.lSexo.AutoSize = true;
            this.lSexo.BackColor = System.Drawing.Color.Transparent;
            this.lSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lSexo.Location = new System.Drawing.Point(301, 408);
            this.lSexo.Name = "lSexo";
            this.lSexo.Size = new System.Drawing.Size(26, 12);
            this.lSexo.TabIndex = 21;
            this.lSexo.Text = "Sexo";
            // 
            // lNombre
            // 
            this.lNombre.AutoSize = true;
            this.lNombre.BackColor = System.Drawing.Color.Transparent;
            this.lNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lNombre.Location = new System.Drawing.Point(185, 360);
            this.lNombre.Name = "lNombre";
            this.lNombre.Size = new System.Drawing.Size(38, 12);
            this.lNombre.TabIndex = 19;
            this.lNombre.Text = "Nombre";
            // 
            // pbFondo
            // 
            this.pbFondo.Image = ((System.Drawing.Image)(resources.GetObject("pbFondo.Image")));
            this.pbFondo.Location = new System.Drawing.Point(391, 296);
            this.pbFondo.Name = "pbFondo";
            this.pbFondo.Size = new System.Drawing.Size(331, 189);
            this.pbFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFondo.TabIndex = 30;
            this.pbFondo.TabStop = false;
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.BackColor = System.Drawing.Color.Transparent;
            this.lblApellidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Location = new System.Drawing.Point(189, 377);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(43, 12);
            this.lblApellidos.TabIndex = 31;
            this.lblApellidos.Text = "Apellidos";
            // 
            // lblLicencia
            // 
            this.lblLicencia.AutoSize = true;
            this.lblLicencia.BackColor = System.Drawing.Color.Transparent;
            this.lblLicencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicencia.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblLicencia.Location = new System.Drawing.Point(314, 371);
            this.lblLicencia.Name = "lblLicencia";
            this.lblLicencia.Size = new System.Drawing.Size(27, 25);
            this.lblLicencia.TabIndex = 32;
            this.lblLicencia.Text = "A";
            // 
            // lblAlergias
            // 
            this.lblAlergias.AutoSize = true;
            this.lblAlergias.BackColor = System.Drawing.Color.Transparent;
            this.lblAlergias.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlergias.Location = new System.Drawing.Point(399, 370);
            this.lblAlergias.Name = "lblAlergias";
            this.lblAlergias.Size = new System.Drawing.Size(39, 12);
            this.lblAlergias.TabIndex = 33;
            this.lblAlergias.Text = "Alergias";
            // 
            // lblExpedicion
            // 
            this.lblExpedicion.AutoSize = true;
            this.lblExpedicion.BackColor = System.Drawing.Color.Transparent;
            this.lblExpedicion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblExpedicion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpedicion.Location = new System.Drawing.Point(244, 416);
            this.lblExpedicion.Name = "lblExpedicion";
            this.lblExpedicion.Size = new System.Drawing.Size(51, 12);
            this.lblExpedicion.TabIndex = 34;
            this.lblExpedicion.Text = "00/00/0000";
            // 
            // lblExpiracion
            // 
            this.lblExpiracion.AutoSize = true;
            this.lblExpiracion.BackColor = System.Drawing.Color.Transparent;
            this.lblExpiracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpiracion.Location = new System.Drawing.Point(137, 450);
            this.lblExpiracion.Name = "lblExpiracion";
            this.lblExpiracion.Size = new System.Drawing.Size(51, 12);
            this.lblExpiracion.TabIndex = 35;
            this.lblExpiracion.Text = "00/00/0000";
            // 
            // gbEmergencia
            // 
            this.gbEmergencia.BackColor = System.Drawing.SystemColors.Menu;
            this.gbEmergencia.Controls.Add(this.txtTelEmergencia);
            this.gbEmergencia.Controls.Add(this.lblTelEmergencia);
            this.gbEmergencia.Controls.Add(this.txtEmergencia);
            this.gbEmergencia.Controls.Add(this.lblAvisar);
            this.gbEmergencia.Location = new System.Drawing.Point(593, 214);
            this.gbEmergencia.Name = "gbEmergencia";
            this.gbEmergencia.Size = new System.Drawing.Size(272, 74);
            this.gbEmergencia.TabIndex = 36;
            this.gbEmergencia.TabStop = false;
            this.gbEmergencia.Text = "Datos de Emergencia";
            // 
            // txtTelEmergencia
            // 
            this.txtTelEmergencia.Location = new System.Drawing.Point(62, 47);
            this.txtTelEmergencia.Name = "txtTelEmergencia";
            this.txtTelEmergencia.Size = new System.Drawing.Size(184, 20);
            this.txtTelEmergencia.TabIndex = 3;
            this.txtTelEmergencia.TextChanged += new System.EventHandler(this.txtTelEmergencia_TextChanged);
            // 
            // lblTelEmergencia
            // 
            this.lblTelEmergencia.AutoSize = true;
            this.lblTelEmergencia.Location = new System.Drawing.Point(7, 50);
            this.lblTelEmergencia.Name = "lblTelEmergencia";
            this.lblTelEmergencia.Size = new System.Drawing.Size(52, 13);
            this.lblTelEmergencia.TabIndex = 2;
            this.lblTelEmergencia.Text = "Teléfono:";
            // 
            // txtEmergencia
            // 
            this.txtEmergencia.Location = new System.Drawing.Point(62, 18);
            this.txtEmergencia.Name = "txtEmergencia";
            this.txtEmergencia.Size = new System.Drawing.Size(184, 20);
            this.txtEmergencia.TabIndex = 1;
            this.txtEmergencia.TextChanged += new System.EventHandler(this.txtEmergencia_TextChanged);
            // 
            // lblAvisar
            // 
            this.lblAvisar.AutoSize = true;
            this.lblAvisar.Location = new System.Drawing.Point(7, 21);
            this.lblAvisar.Name = "lblAvisar";
            this.lblAvisar.Size = new System.Drawing.Size(48, 13);
            this.lblAvisar.TabIndex = 0;
            this.lblAvisar.Text = "Avisar a:";
            // 
            // lblEmergencia
            // 
            this.lblEmergencia.AutoSize = true;
            this.lblEmergencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmergencia.Location = new System.Drawing.Point(397, 394);
            this.lblEmergencia.Name = "lblEmergencia";
            this.lblEmergencia.Size = new System.Drawing.Size(54, 12);
            this.lblEmergencia.TabIndex = 37;
            this.lblEmergencia.Text = "Emergencia";
            // 
            // lblTelEmer
            // 
            this.lblTelEmer.AutoSize = true;
            this.lblTelEmer.BackColor = System.Drawing.Color.Transparent;
            this.lblTelEmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelEmer.Location = new System.Drawing.Point(586, 381);
            this.lblTelEmer.Name = "lblTelEmer";
            this.lblTelEmer.Size = new System.Drawing.Size(40, 12);
            this.lblTelEmer.TabIndex = 38;
            this.lblTelEmer.Text = "Teléfono";
            // 
            // sigPlusNET1
            // 
/*            this.sigPlusNET1.BackColor = System.Drawing.Color.White;
            this.sigPlusNET1.ForeColor = System.Drawing.Color.Black;
            this.sigPlusNET1.Location = new System.Drawing.Point(415, 409);
            this.sigPlusNET1.Name = "sigPlusNET1";
            this.sigPlusNET1.Size = new System.Drawing.Size(71, 17);
            this.sigPlusNET1.TabIndex = 39;
            this.sigPlusNET1.Text = "sigPlusNET1";*/
            // 
            // gbFirmas
            // 
            this.gbFirmas.BackColor = System.Drawing.SystemColors.Menu;
            this.gbFirmas.Controls.Add(this.button4);
            this.gbFirmas.Controls.Add(this.btnLimpiar);
            this.gbFirmas.Controls.Add(this.btnDetener);
            this.gbFirmas.Controls.Add(this.bntIniciar);
            this.gbFirmas.Location = new System.Drawing.Point(728, 315);
            this.gbFirmas.Name = "gbFirmas";
            this.gbFirmas.Size = new System.Drawing.Size(137, 166);
            this.gbFirmas.TabIndex = 40;
            this.gbFirmas.TabStop = false;
            this.gbFirmas.Text = "Opciones para firma";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(14, 129);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(105, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Guardar";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(14, 93);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(105, 23);
            this.btnLimpiar.TabIndex = 2;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnDetener
            // 
            this.btnDetener.Location = new System.Drawing.Point(14, 56);
            this.btnDetener.Name = "btnDetener";
            this.btnDetener.Size = new System.Drawing.Size(105, 23);
            this.btnDetener.TabIndex = 1;
            this.btnDetener.Text = "Detener";
            this.btnDetener.UseVisualStyleBackColor = true;
            this.btnDetener.Click += new System.EventHandler(this.btnDetener_Click);
            // 
            // bntIniciar
            // 
            this.bntIniciar.Location = new System.Drawing.Point(14, 20);
            this.bntIniciar.Name = "bntIniciar";
            this.bntIniciar.Size = new System.Drawing.Size(105, 23);
            this.bntIniciar.TabIndex = 0;
            this.bntIniciar.Text = "Iniciar";
            this.bntIniciar.UseVisualStyleBackColor = true;
            this.bntIniciar.Click += new System.EventHandler(this.bntIniciar_Click);
            // 
            // pbFirma1
            // 
            this.pbFirma1.BackColor = System.Drawing.Color.Transparent;
            this.pbFirma1.Location = new System.Drawing.Point(261, 429);
            this.pbFirma1.Name = "pbFirma1";
            this.pbFirma1.Size = new System.Drawing.Size(88, 25);
            this.pbFirma1.TabIndex = 41;
            this.pbFirma1.TabStop = false;
            // 
            // pbFirma2
            // 
            this.pbFirma2.Location = new System.Drawing.Point(537, 399);
            this.pbFirma2.Name = "pbFirma2";
            this.pbFirma2.Size = new System.Drawing.Size(100, 27);
            this.pbFirma2.TabIndex = 42;
            this.pbFirma2.TabStop = false;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(871, 493);
            this.Controls.Add(this.pbFirma2);
            this.Controls.Add(this.pbFirma1);
            this.Controls.Add(this.gbFirmas);
//            this.Controls.Add(this.sigPlusNET1);
            this.Controls.Add(this.lblTelEmer);
            this.Controls.Add(this.lblEmergencia);
            this.Controls.Add(this.gbEmergencia);
            this.Controls.Add(this.lblExpiracion);
            this.Controls.Add(this.lblExpedicion);
            this.Controls.Add(this.lblAlergias);
            this.Controls.Add(this.lblLicencia);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.pbCodeBar);
            this.Controls.Add(this.pbCaptura);
            this.Controls.Add(this.pbImagen);
            this.Controls.Add(this.lDireccion);
            this.Controls.Add(this.lLote);
            this.Controls.Add(this.lCurp);
            this.Controls.Add(this.lSexo);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.lNombre);
            this.Controls.Add(this.pbImagenFondo);
            this.Controls.Add(this.tsPrincipal);
            this.Controls.Add(this.gbDatosGenerales);
            this.Controls.Add(this.gbDireccion);
            this.Controls.Add(this.pbFondo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Captura Datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.gbDireccion.ResumeLayout(false);
            this.gbDireccion.PerformLayout();
            this.gbDatosGenerales.ResumeLayout(false);
            this.gbDatosGenerales.PerformLayout();
            this.tsPrincipal.ResumeLayout(false);
            this.tsPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenFondo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCodeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCaptura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).EndInit();
            this.gbEmergencia.ResumeLayout(false);
            this.gbEmergencia.PerformLayout();
            this.gbFirmas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFirma1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFirma2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.GroupBox gbDireccion;
        private System.Windows.Forms.GroupBox gbDatosGenerales;
        private System.Windows.Forms.ToolStrip tsPrincipal;
        private System.Windows.Forms.ToolStripButton tsbLimpiar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblSangre;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.ComboBox cmbLote;
        private System.Windows.Forms.ComboBox cmbTipoLicencia;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.PictureBox pbImagenFondo;
        private System.Windows.Forms.PictureBox pbCodeBar;
        private System.Windows.Forms.PictureBox pbImagen;
        private System.Windows.Forms.PictureBox pbCaptura;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.OpenFileDialog ofdAbrirImagen;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.Label lDireccion;
        private System.Windows.Forms.Label lLote;
        private System.Windows.Forms.Label lCurp;
        private System.Windows.Forms.Label lSexo;
        private System.Windows.Forms.Label lNombre;
        private System.Windows.Forms.Label lblsexo;
        private System.Windows.Forms.RadioButton rbFemenino;
        private System.Windows.Forms.RadioButton rbMasculino;
        private System.Windows.Forms.PictureBox pbFondo;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLicencia;
        private System.Windows.Forms.DateTimePicker dtpExpedicion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpExpiracion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAlergias;
        private System.Windows.Forms.Label lblAlergias;
        private System.Windows.Forms.Label lblExpedicion;
        private System.Windows.Forms.Label lblExpiracion;
        private System.Windows.Forms.GroupBox gbEmergencia;
        private System.Windows.Forms.TextBox txtEmergencia;
        private System.Windows.Forms.Label lblAvisar;
        private System.Windows.Forms.Label lblEmergencia;
        private System.Windows.Forms.TextBox txtTelEmergencia;
        private System.Windows.Forms.Label lblTelEmergencia;
        private System.Windows.Forms.Label lblTelEmer;
        private Topaz.SigPlusNET sigPlusNET1;
        private System.Windows.Forms.GroupBox gbFirmas;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnDetener;
        private System.Windows.Forms.Button bntIniciar;
        private System.Windows.Forms.ToolStripButton tsbEditarFirma;
        private System.Windows.Forms.PictureBox pbFirma1;
        private System.Windows.Forms.PictureBox pbFirma2;
       
    }
}

