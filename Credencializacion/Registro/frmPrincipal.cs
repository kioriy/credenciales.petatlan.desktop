﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp;
using AForge;
using AForge.Video;
using AForge.Video.DirectShow;

namespace Registro
{
    public partial class frmPrincipal : Form
    {
        CodeBar cb = new CodeBar();
        DataSet obtenDatos; 
        BD bd = new BD();
        Video VD;
        int entra;
        int id_trabajador;
        //private PrintDocument imprimirDoc;
        Bitmap memoria_Imagen;

        public frmPrincipal()
        {
            InitializeComponent();

           /* string sqlConsulta1 = "Select nombre || ' ' || apellido_p || ' ' || apellido_m AS nombre_completo from trabajador";
            string sqlConsulta2 = "Select * from coto";*/
            //bd.llenarCombobox(cbNombre, sqlConsulta1, "nombre", "trabajador", ref entra);
           // bd.llenarCombobox(cmbTipoLicencia, sqlConsulta2, "numero", "coto", ref entra);
           
         //   lPuesto.Text = ""; lConstructora.Text = ""; lCoto.Text = ""; lLote.Text = ""; lDireccion.Text = "";
            dtpFecha.Value = DateTime.Now;
            dtpExpedicion.Value = DateTime.Now;
            dtpExpiracion.Value = DateTime.Now;
            Transparencia();
            llenadoTipoSangre();
            llenadotipoLicencia();
            sigPlusNET1.SetDisplayPenWidth(1);
            sigPlusNET1.SetImagePenWidth(1);
        }

        private void llenadotipoLicencia()
        {
            cmbTipoLicencia.Items.Add("A");
            cmbTipoLicencia.Items.Add("B");
            cmbTipoLicencia.Items.Add("C");
        }
        private void llenadoTipoSangre()
        {
            cmbLote.Items.Add("O-");
            cmbLote.Items.Add("O+");
            cmbLote.Items.Add("B-");
            cmbLote.Items.Add("B+");
            cmbLote.Items.Add("A-");
            cmbLote.Items.Add("A+");
            cmbLote.Items.Add("AB-");
            cmbLote.Items.Add("AB+");
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            VD = new Video(pbImagen, pbCaptura); 
            VD.cargarVideo();
            pbCaptura.Hide(); 
           
        }
     
        //<<VALIDACIONES
        //private void cbNombre_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space) && (e.KeyChar != (char)Keys.Enter))
        //    {
        //        MessageBox.Show("Sólo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); e.Handled = true;
               
        //        return;
        //    }
        //    else if (e.KeyChar == (char)Keys.Enter) 
        //    {
        //        txtapellido_p.Focus();               
        //    }
        //}

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space) && (e.KeyChar != (char)Keys.Enter))
            {
                MessageBox.Show("Sólo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); e.Handled = true;
                return;
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                txtNombre.Text = txtNombre.Text.ToUpper().Trim();

                generaraCodBar128();
                try
                {
                    string nombre = obtenDatos.Tables["trabajador"].Rows[0]["nombre"].ToString();
                    
                    if (nombre != txtNombre.Text)
                    {
                        updateDatos("nombre", txtNombre.Text);
                        obtenDatos.Tables["trabajador"].Rows[0]["nombre"] = txtNombre.Text;
                    }
                }

                catch (Exception) { }
            }
        }

        private void txtapellido_p_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space) && (e.KeyChar != (char)Keys.Enter))
            {
                MessageBox.Show("Sólo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); e.Handled = true;
                return;
            }
        }




     /*   private void txtConstructora_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space) && (e.KeyChar != (char)Keys.Enter))
            {
                MessageBox.Show("Sólo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); e.Handled = true;
                return;
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                cmbCoto.Focus();
            }
        }*/


        //VALIDACIONES >>        

        //METODO PARA GENERAR CURP 
    /*    private string genera_curp()
        {
            string curp = txtApellido.Text.Substring(0, 1);
            string fecha = dtpFecha.Value.ToString("dd-MM-yyyy");
            int i;
            for (i = 1; i < txtApellido.TextLength; i++)
            {
                if (txtApellido.Text.Substring(i, 1) == "A"
                    || txtApellido.Text.Substring(i, 1) == "O"
                    || txtApellido.Text.Substring(i, 1) == "E"
                    || txtApellido.Text.Substring(i, 1) == "U"
                    || txtApellido.Text.Substring(i, 1) == "I")
                {
                    curp = curp + txtApellido.Text.Substring(i, 1);
                    break;
                }
            }

            curp = curp + txtNombre.Text.Substring(0, 1)
                   + fecha.Substring(8, 2) + fecha.Substring(3, 2)
                   + fecha.Substring(0, 2); fecha.Substring(8, 2);

           // Random rnd= new Random();
            //int x = (rnd.Next(999));

            return curp;//+ x.ToString();
        }*/

        //Carga los datos al seleccionar un nombre del ListBox
       
        //private void cbNombre_SelectedIndexChanged(object sender, EventArgs e)
        //{
                       
        //    if (entra != 1)
        //    {
        //        string nombre = cbNombre.Text;
        //        string sqlConsulta = "Select id_trabajador,curp,nombre,apellido_p,apellido_m,puesto,constructora,calle,numero,colonia,fk_id_coto,fk_id_lote from trabajador where nombre = '" + nombre + "'";
        //        obtenDatos = new DataSet();
        //        bd.adaptador = bd.consultaAdaptador(sqlConsulta);
        //        obtenDatos.Reset();
        //        bd.adaptador.Fill(obtenDatos, "trabajador");

        //            id_trabajador = Convert.ToInt32(obtenDatos.Tables["trabajador"].Rows[0]["id_trabajador"]);
        //            string curp = obtenDatos.Tables["trabajador"].Rows[0]["curp"].ToString();
        //            string nombre_completo = obtenDatos.Tables["trabajador"].Rows[0]["nombre"].ToString();
        //            string ap_p = obtenDatos.Tables["trabajador"].Rows[0]["apellido_p"].ToString();
        //            string ap_m = obtenDatos.Tables["trabajador"].Rows[0]["apellido_m"].ToString();
        //            string puesto = obtenDatos.Tables["trabajador"].Rows[0]["puesto"].ToString();
        //            string constructora = obtenDatos.Tables["trabajador"].Rows[0]["constructora"].ToString();
        //            string calle = obtenDatos.Tables["trabajador"].Rows[0]["calle"].ToString();
        //            string numero = obtenDatos.Tables["trabajador"].Rows[0]["numero"].ToString();
        //            string colonia = obtenDatos.Tables["trabajador"].Rows[0]["colonia"].ToString();
        //            string coto = obtenDatos.Tables["trabajador"].Rows[0]["fk_id_coto"].ToString();
        //            string lote = obtenDatos.Tables["trabajador"].Rows[0]["fk_id_lote"].ToString();
        //            txtapellido_p.Text = ap_p; txtapellido_m.Text = ap_m;
        //            txtPuesto.Text = puesto; txtConstructora.Text = constructora;
        //            txtCalle.Text = calle; txtNumero.Text = numero;
        //            txtColonia.Text = colonia; cmbCoto.Text = coto; cmbLote.Text = lote;
        //            dtpFecha.Value = Convert.ToDateTime(carga_fecha(curp));
        //            generaraCodBar128();           txtapellido_p.Focus();   
        //    }


        //}

            

        //Cambia de foco al seleccionar un lote
        private void cmbLote_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCalle.Focus();
        }


        private void cmbLote_TextChanged(object sender, EventArgs e)
        {
            lLote.Text = "";
            lLote.Text = cmbLote.Text;
        }

        private void txtCalle_TextChanged(object sender, EventArgs e)
        {
            if (txtNumero.Text != "")
            {
                lDireccion.Text = txtCalle.Text + " # " + txtNumero.Text;
            }
            else
            {
                lDireccion.Text = txtCalle.Text;
            }
        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {
            lDireccion.Text = txtCalle.Text + " # " + txtNumero.Text;
        }

        private void lblDireccion_TextChanged(object sender, EventArgs e)
        {
            if (lDireccion.Text.Length > 35)
            {
                lDireccion.Text = txtCalle.Text + "\n" + " # " + txtNumero.Text;
            }
         }
        //Se visualizan los datos en pantalla>>

        private void tsbImprimir_Click(object sender, EventArgs e)
        {
            txtCurp.Focus();
            //bool respuesta=false;
            bool reimpresion;

            if (evaluarControles(true) ) 
            {
                BD bd = new BD();

                if (txtNombre.Text != "" && txtApellido.Text != "" && txtCurp.Text != "" && txtCalle.Text != "" && txtNumero.Text != "")
                {
                    string fechaAlta = DateTime.Now.ToString();
                    //string curp = genera_curp();
                    string nombre = txtNombre.Text.Trim(); 
                    string ape_p = txtApellido.Text.Trim(); 
                    string puesto = txtCurp.Text.Trim(); 
                    string calle = txtCalle.Text.Trim(); string num = txtNumero.Text.Trim(); 

                    if (pbCaptura.Image != null)
                    {
                        
                        if (obtenDatos.Tables[0].Rows.Count > 0)
                        {
                            reimpresion = true;
                        }
                        else { reimpresion = false; }
                       
                       
                            
                            //string path = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".jpg";
                            Captura_pantalla();
                            //pbPrueba.Image = memoria_Imagen; pbPrueba.Image.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);

                            PrintDialog printDialog1 = new PrintDialog();
                            printDialog1.Document = printDocument1;
                            DialogResult result = printDialog1.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                printDocument1.Print();
                                if (reimpresion==true)
                                {                                                                        
                                    tsbImprimir.Enabled = false;
                                    //System.IO.File.Delete("C:\\fotos\\" + id_trabajador.ToString() + ".jpg");
                                    pbCaptura.Image.Save("C:\\fotos\\" + id_trabajador.ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg); 
                                }

                                if (reimpresion==false)
                                {
                                    bool insercion = bd.ejecuta_comando("INSERT INTO `TRABAJADOR` (`curp`,`nombre`,`apellido_p`,`puesto`, calle`,`numero`,`colonia`,`alta`,`fk_id_coto`,`fk_id_lote`) VALUES ('" + nombre + "','" + ape_p + "', '" + puesto + "','" + calle + "','" + num + "','" + fechaAlta + "'," + cmbTipoLicencia.Text + "," + cmbLote.Text + ")");

                                  
                                    if (insercion == true)
                                    {
                                        string sqlConsulta = "SELECT MAX(id_trabajador) AS id FROM trabajador";
                                        obtenDatos = new DataSet();
                                        bd.adaptador = bd.consultaAdaptador(sqlConsulta);
                                        obtenDatos.Reset();
                                        bd.adaptador.Fill(obtenDatos, "trabajador");

                                        string id_trab = Convert.ToString(obtenDatos.Tables["trabajador"].Rows[0]["id"]);

                                        if (!Directory.Exists("C:\\fotos"))
                                        {
                                              System.IO.Directory.CreateDirectory("C:\\fotos");
                                        }
                                   
                                        pbCaptura.Image.Save("C:\\fotos\\" + id_trab + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);                                       
                                        MessageBox.Show("Datos Generales insertados correctamente"); tsbImprimir.Enabled = false;
                                    }
                                }
                            }

                    }

                    else
                    {
                        MessageBox.Show("Favor de tomar una foto para la credencial"); 
                    }

                }
                else
                {
                    MessageBox.Show("Necesita llenar todos los datos para hacer un registro");
                }
               
            }
           
        }

        private void tsbLimpiar_Click(object sender, EventArgs e)
        {
           
            txtNombre.Clear();
            txtApellido.Clear(); txtCurp.Clear(); 
            entra = 1;
            cmbTipoLicencia.SelectedIndex = -1; 
            entra = 0;
            cmbLote.Text = ""; 
            txtCalle.Clear(); 
            txtNumero.Clear(); 
            pbCodeBar.Image = null;
            pbCaptura.Image = null; pbCaptura.Hide();
            lNombre.Text = ""; lSexo.Text = ""; lCurp.Text = ""; lLote.Text = ""; lDireccion.Text = "";
            dtpFecha.Value = DateTime.Now;
            if (obtenDatos != null)
            {
                obtenDatos.Clear();
            }
            id_trabajador = -1;
            tsbImprimir.Enabled = true; 
        }

        //Genera el código de barras
        private void generaraCodBar128() 
        {
            if (evaluarControles(false)) 
            {
                string path = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".gif";
                //Directory.GetCurrentDirectory();

                //cb.Code128(genera_curp(), path);
                
                //pbCodeBar.Image = Image.FromFile(path);
            }
        }

        /// <summary>
        /// Este metodo es para ver si los campos estan llenos
        /// </summary>
        /// <param name="bandera"></param>
        /// <returns></returns>

        private bool evaluarControles(bool bandera) 
        {
            bool controlesOk = true;

            if (DateTime.Compare((DateTime)dtpFecha.Value, DateTime.Today) >= 0)
            {
                controlesOk = false;

                if (bandera)
                {
                    MessageBox.Show("favor de ingresar una fecha valida");
                }
                return controlesOk;
            }
            else if (txtNombre.Text.Trim() == "")
            {
                controlesOk = false;

                if (bandera)
                {
                    MessageBox.Show("Favor de llenar nombres");
                }
                return controlesOk;
            }

           /* else if (txtapellido_m.Text.Trim() == "")
            {
                controlesOk = false;

                if (bandera)
                {
                    MessageBox.Show("Favor de llenar apellido materno");
                }
                return controlesOk;
            }*/
            
            else if (txtApellido.Text.Trim() == "")
            {
                controlesOk = false;

                if (bandera)
                {
                    MessageBox.Show("Favor de llenar apellido paterno");
                }
                return controlesOk;
            }

            return controlesOk;
        }

        //<<Eventos que se desencadenan al salir del foco

        //private void cbNombre_Leave(object sender, EventArgs e)
        //{
            
        //    cbNombre.Text = cbNombre.Text.ToUpper().Trim();

        //    generaraCodBar128();
        //    try
        //    {
        //        string nombre = obtenDatos.Tables["trabajador"].Rows[0]["nombre"].ToString();
        //        string nombreIndex = "";
        //        if (nombre != cbNombre.Text)
        //        {
        //            updateDatos("nombre", cbNombre.Text);
        //            nombreIndex = cbNombre.Text;
        //            bd.llenarCombobox(cbNombre, "Select * from trabajador", "nombre", "trabajador", ref entra);
        //            int index = cbNombre.FindStringExact(nombreIndex);
        //            cbNombre.SelectedIndex = index;
        //            generaraCodBar128();
                    
        //        }
        //    }

        //    catch (Exception) { }   
        //}

        //private void txtNombre_Leave_1(object sender, EventArgs e)
        //{
        //    txtNombre.Text = txtNombre.Text.ToUpper().Trim();

        //    generaraCodBar128();
        //    try
        //    {
        //        string nombre = obtenDatos.Tables["trabajador"].Rows[0]["nombre"].ToString();
        //        // string nombreIndex = "";
        //        if (nombre != txtNombre.Text)
        //        {
        //            updateDatos("nombre", txtNombre.Text);
        //            obtenDatos.Tables["trabajador"].Rows[0]["apellido_p"] = txtapellido_p.Text;
        //            //bd.llenarCombobox(cbNombre, "Select * from trabajador", "nombre", "trabajador", ref entra);
        //            //int index = cbNombre.FindStringExact(nombreIndex);
        //            //cbNombre.SelectedIndex = index;


        //        }
        //    }

        //    catch (Exception) { }
        //    lbNombres.Visible = false;
        //}


        private void txtapellido_p_Leave(object sender, EventArgs e)
        {
            
            generaraCodBar128();
            if (txtApellido.Text != "")
            {
                try
                {
                    string ap_p = obtenDatos.Tables["trabajador"].Rows[0]["apellido_p"].ToString();

                    if (ap_p != txtApellido.Text)
                    {
                        updateDatos("apellido_p", txtApellido.Text);
                        obtenDatos.Tables["trabajador"].Rows[0]["apellido_p"] = txtApellido.Text; //actualizacion de apellido
                    }
                }

                catch (Exception) { }
            }
        }

        private void txtapellido_m_Leave(object sender, EventArgs e)
        {
            generaraCodBar128();
            /*if (txtapellido_m.Text != "")
            {
                try
                {
                    string ap_m = obtenDatos.Tables["trabajador"].Rows[0]["apellido_m"].ToString();

                    if (ap_m != txtapellido_m.Text)
                    {
                        updateDatos("apellido_m", txtapellido_m.Text); obtenDatos.Tables["trabajador"].Rows[0]["apellido_p"] = txtapellido_m.Text;
                    }
                }

                catch (Exception) { }
            }*/
        }

        private void txtPuesto_Leave(object sender, EventArgs e)
        {
            if (txtCurp.Text != "")
            {
                try
                {
                    string puesto = obtenDatos.Tables["trabajador"].Rows[0]["puesto"].ToString();

                    if (puesto != txtCurp.Text)
                    {
                        updateDatos("puesto", txtCurp.Text);
                    }
                }

                catch (Exception) { }
            }
        }

     /*   private void txtConstructora_Leave(object sender, EventArgs e)
        {
            if (txtConstructora.Text != "")
            {
                try
                {
                    string Constructora = obtenDatos.Tables["trabajador"].Rows[0]["constructora"].ToString();

                    if (Constructora != txtConstructora.Text)
                    {
                        updateDatos("constructora", txtConstructora.Text); obtenDatos.Tables["trabajador"].Rows[0]["constructora"] = txtConstructora.Text;
                    }
                }

                catch (Exception) { }
            }
        }*/

        private void cmbCoto_Leave(object sender, EventArgs e)
        {
            try
            {
                string coto = obtenDatos.Tables["trabajador"].Rows[0]["fk_id_coto"].ToString();

                if (coto != cmbTipoLicencia.Text)
                {
                    updateDatos("fk_id_coto", cmbTipoLicencia.Text); obtenDatos.Tables["trabajador"].Rows[0]["fk_id_coto"] = cmbTipoLicencia.Text;
                }
            }

            catch (Exception) { }    
        }

        private void cmbLote_Leave(object sender, EventArgs e)
        {
            try
            {
                string lote = obtenDatos.Tables["trabajador"].Rows[0]["fk_id_lote"].ToString();

                if (lote != cmbLote.Text)
                {
                    updateDatos("fk_id_lote", cmbLote.Text); obtenDatos.Tables["trabajador"].Rows[0]["fk_id_lote"] = cmbLote.Text;
                }
            }

            catch (Exception) { }   
        }

        private void txtCalle_Leave(object sender, EventArgs e)
        {
            if (txtCalle.Text != "")
            {
                try
                {
                    string calle = obtenDatos.Tables["trabajador"].Rows[0]["calle"].ToString();

                    if (calle != txtCalle.Text)
                    {
                        updateDatos("calle", txtCalle.Text); obtenDatos.Tables["trabajador"].Rows[0]["calle"] = txtCalle.Text;
                    }
                }

                catch (Exception) { }
            }
        }

        private void txtNumero_Leave(object sender, EventArgs e)
        {
            if (txtNumero.Text != "")
            {
                try
                {
                    string num = obtenDatos.Tables["trabajador"].Rows[0]["numero"].ToString();

                    if (num != txtNumero.Text)
                    {
                        updateDatos("numero", txtNumero.Text); obtenDatos.Tables["trabajador"].Rows[0]["numero"] = txtNumero.Text;
                    }
                }

                catch (Exception) { }
            }
        }


        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            //generaraCodBar128();
            lblFecha.Text = dtpFecha.Text.ToString();
        }

       //Actualiza datos
        private void updateDatos(string campo, string set) 
        {
            bool respuesta;
            string query = "UPDATE  `credencializacion`.`trabajador` SET  `"+campo+"` =  '" + set + "' WHERE  `trabajador`.`id_trabajador` =  " + id_trabajador + "";
            
            respuesta = bd.ejecuta_comando(query);

            if (respuesta)
            {
                MessageBox.Show("Cambio realizado");
            }
        }

        //Carga Fecha de nacimiento a partir de la curp
        private string carga_fecha(string curp)
        {
            string cadena = curp;
            cadena = cadena.Substring(8, 1) + cadena.Substring(9, 1) + "/"+ cadena.Substring(6, 1) +
                     cadena.Substring(7, 1)  +"/" +  cadena.Substring(4, 1) + cadena.Substring(5, 1);
            return cadena; 
        }

        //Captura de fotos
        private void pbImagen_Click(object sender, EventArgs e)
        {
                VD.capturarImagen();
                pbCaptura.Show();
        }

        private void pbCaptura_Click(object sender, EventArgs e)
        {
            pbImagen.Show(); pbCaptura.Hide();

            pbCaptura.Image = null  ;
            try
            {
                if (System.IO.File.Exists("C:\\fotos\\" + id_trabajador.ToString() + ".jpg"))
                {
                    System.IO.File.Delete("C:\\fotos\\" + id_trabajador.ToString() + ".jpg");
                }
            }
            catch (Exception) { }
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {

            e.Graphics.DrawImage(memoria_Imagen, 111.5f, 40);
        }

        private void Captura_pantalla()
        {
            Graphics myGraphics = pbImagenFondo.CreateGraphics();
            Size s = pbImagenFondo.Size;
            memoria_Imagen = new Bitmap( s.Width, s.Height,myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoria_Imagen);
            int CoorX = this.Location.X + 122;
            int CoorY = this.Location.Y + 330;
            memoryGraphics.CopyFromScreen(CoorX, CoorY, 0, 0, s);
           
        }


        //Cerrando el formulario
        private void frmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            VD.detenerVideo();
        }

       
        private void Transparencia() 
        {
           /* pos = pbImagenFondo.PointToClient(pos);

            pos = this.PointToScreen(lNombre.Location);
            pos = pbImagenFondo.PointToClient(pos);
            lNombre.Parent = pbImagenFondo;
            lNombre.Location = pos;
            lNombre.BackColor = Color.Transparent;

            pos = this.PointToScreen(lConstructora.Location);
            pos = pbImagenFondo.PointToClient(pos);
            lConstructora.Parent = pbImagenFondo;
            lConstructora.Location = pos;
            lConstructora.BackColor = Color.Transparent;

            pos = this.PointToScreen(lPuesto.Location);
            pos = pbImagenFondo.PointToClient(pos);
            lPuesto.Parent = pbImagenFondo;
            lPuesto.Location = pos;
            lPuesto.BackColor = Color.Transparent;

            pos = this.PointToScreen(lCoto.Location);
            pos = pbImagenFondo.PointToClient(pos);
            lCoto.Parent = pbImagenFondo;
            lCoto.Location = pos;
            lCoto.BackColor = Color.Transparent;

            pos = this.PointToScreen(lLote.Location);
            pos = pbImagenFondo.PointToClient(pos);
            lLote.Parent = pbImagenFondo;
            lLote.Location = pos;
            lLote.BackColor = Color.Transparent;

            pos = this.PointToScreen(lDireccion.Location);
            pos = pbImagenFondo.PointToClient(pos);
            lDireccion.Parent = pbImagenFondo;
            lDireccion.Location = pos;
            lDireccion.BackColor = Color.Transparent;*/
        }

        private void tsmFrente_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdAbrirImagen.ShowDialog() == DialogResult.OK)
                {
                    string imagen = ofdAbrirImagen.FileName;
                    pbImagenFondo.Image = Image.FromFile(imagen);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
            }
        }

        private void tsbCargarCotos_Click(object sender, EventArgs e)
        {
            frmAgregarCoto agregarCoto = new frmAgregarCoto();

            agregarCoto.Show();
        }

        private void rbMasculino_CheckedChanged(object sender, EventArgs e)
        {
            if(rbMasculino.Checked)
            {
                lSexo.Location = new System.Drawing.Point (297,408);
                lSexo.Text = "MASCULINO";
            }
        }

        private void rbFemenino_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFemenino.Checked)
            {
                lSexo.Location = new System.Drawing.Point(301, 408);
                lSexo.Text = "FEMENINO";
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            lNombre.Text = "";
            lNombre.Text = txtNombre.Text.ToUpper();
        }

        private void txtapellido_p_TextChanged(object sender, EventArgs e)
        {
            lblApellidos.Text = "";
            lblApellidos.Text = txtApellido.Text.ToUpper();
        }

        private void txtCurp_TextChanged(object sender, EventArgs e)
        {
            lCurp.Text = "";
            lCurp.Text = txtCurp.Text.ToUpper();
        }

        private void cmbTipoLicencia_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbTipoLicencia.Text == "A")
            {
                lblLicencia.Text = "A";
                lblLicencia.ForeColor = Color.Aqua;
            }
            if (cmbTipoLicencia.Text == "B")
            {
                lblLicencia.Text = "B";
                lblLicencia.ForeColor = Color.YellowGreen;
            }
            if (cmbTipoLicencia.Text == "C")
            {
                lblLicencia.Text = "C";
                lblLicencia.ForeColor = Color.Red;
            }
        }

        private void txtAlergias_TextChanged(object sender, EventArgs e)
        {
            lblAlergias.Text = txtAlergias.Text.ToUpper();
        }

        private void dtpExpedicion_ValueChanged(object sender, EventArgs e)
        {
            lblExpedicion.Text = dtpExpedicion.Text.ToString();
        }

        private void dtpExpiracion_ValueChanged(object sender, EventArgs e)
        {
            lblExpiracion.Text = dtpExpiracion.Text.ToString();
        }

        private void txtEmergencia_TextChanged(object sender, EventArgs e)
        {
            lblEmergencia.Text = txtEmergencia.Text.ToUpper();
        }

        private void txtTelEmergencia_TextChanged(object sender, EventArgs e)
        {
            lblTelEmer.Text = txtTelEmergencia.Text.ToUpper();
        }

        private void bntIniciar_Click(object sender, EventArgs e)
        {
            sigPlusNET1.SetTabletState(1);
        }

        private void btnDetener_Click(object sender, EventArgs e)
        {
            sigPlusNET1.SetTabletState(0);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            sigPlusNET1.ClearTablet();
        }

        private void tsbEditarFirma_Click(object sender, EventArgs e)
        {
            frmAgregarCoto firmas = new frmAgregarCoto();
            firmas.Show();
        }

    }
}