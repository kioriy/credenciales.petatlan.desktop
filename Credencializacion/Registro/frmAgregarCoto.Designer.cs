﻿namespace Registro
{
    partial class frmAgregarCoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnDetener = new System.Windows.Forms.Button();
            this.bntIniciar = new System.Windows.Forms.Button();
            this.sigPlusNET1 = new Topaz.SigPlusNET();
            this.sigPlusNET2 = new Topaz.SigPlusNET();
            this.btnGuardar2 = new System.Windows.Forms.Button();
            this.btnLimpiar2 = new System.Windows.Forms.Button();
            this.btnDetener2 = new System.Windows.Forms.Button();
            this.btnIniciar2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sigPlusNET1);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.btnLimpiar);
            this.groupBox1.Controls.Add(this.btnDetener);
            this.groupBox1.Controls.Add(this.bntIniciar);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(293, 198);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Presidente Municipal";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnGuardar2);
            this.groupBox2.Controls.Add(this.sigPlusNET2);
            this.groupBox2.Controls.Add(this.btnLimpiar2);
            this.groupBox2.Controls.Add(this.btnDetener2);
            this.groupBox2.Controls.Add(this.btnIniciar2);
            this.groupBox2.Location = new System.Drawing.Point(311, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(308, 198);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Director Seg. Pública";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(32, 169);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(105, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = "Guardar";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(143, 169);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(105, 23);
            this.btnLimpiar.TabIndex = 6;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnDetener
            // 
            this.btnDetener.Location = new System.Drawing.Point(143, 140);
            this.btnDetener.Name = "btnDetener";
            this.btnDetener.Size = new System.Drawing.Size(105, 23);
            this.btnDetener.TabIndex = 5;
            this.btnDetener.Text = "Detener";
            this.btnDetener.UseVisualStyleBackColor = true;
            this.btnDetener.Click += new System.EventHandler(this.btnDetener_Click);
            // 
            // bntIniciar
            // 
            this.bntIniciar.Location = new System.Drawing.Point(32, 140);
            this.bntIniciar.Name = "bntIniciar";
            this.bntIniciar.Size = new System.Drawing.Size(105, 23);
            this.bntIniciar.TabIndex = 4;
            this.bntIniciar.Text = "Iniciar";
            this.bntIniciar.UseVisualStyleBackColor = true;
            this.bntIniciar.Click += new System.EventHandler(this.bntIniciar_Click);
            // 
            // sigPlusNET1
            // 
            this.sigPlusNET1.BackColor = System.Drawing.Color.White;
            this.sigPlusNET1.ForeColor = System.Drawing.Color.Black;
            this.sigPlusNET1.Location = new System.Drawing.Point(32, 22);
            this.sigPlusNET1.Name = "sigPlusNET1";
            this.sigPlusNET1.Size = new System.Drawing.Size(216, 104);
            this.sigPlusNET1.TabIndex = 40;
            this.sigPlusNET1.Text = "sigPlusNET1";
            // 
            // sigPlusNET2
            // 
            this.sigPlusNET2.BackColor = System.Drawing.Color.White;
            this.sigPlusNET2.ForeColor = System.Drawing.Color.Black;
            this.sigPlusNET2.Location = new System.Drawing.Point(45, 22);
            this.sigPlusNET2.Name = "sigPlusNET2";
            this.sigPlusNET2.Size = new System.Drawing.Size(216, 104);
            this.sigPlusNET2.TabIndex = 41;
            this.sigPlusNET2.Text = "sigPlusNET2";
            // 
            // btnGuardar2
            // 
            this.btnGuardar2.Location = new System.Drawing.Point(45, 169);
            this.btnGuardar2.Name = "btnGuardar2";
            this.btnGuardar2.Size = new System.Drawing.Size(105, 23);
            this.btnGuardar2.TabIndex = 44;
            this.btnGuardar2.Text = "Guardar";
            this.btnGuardar2.UseVisualStyleBackColor = true;
            this.btnGuardar2.Click += new System.EventHandler(this.btnGuardar2_Click);
            // 
            // btnLimpiar2
            // 
            this.btnLimpiar2.Location = new System.Drawing.Point(156, 169);
            this.btnLimpiar2.Name = "btnLimpiar2";
            this.btnLimpiar2.Size = new System.Drawing.Size(105, 23);
            this.btnLimpiar2.TabIndex = 43;
            this.btnLimpiar2.Text = "Limpiar";
            this.btnLimpiar2.UseVisualStyleBackColor = true;
            this.btnLimpiar2.Click += new System.EventHandler(this.btnLimpiar2_Click);
            // 
            // btnDetener2
            // 
            this.btnDetener2.Location = new System.Drawing.Point(156, 140);
            this.btnDetener2.Name = "btnDetener2";
            this.btnDetener2.Size = new System.Drawing.Size(105, 23);
            this.btnDetener2.TabIndex = 42;
            this.btnDetener2.Text = "Detener";
            this.btnDetener2.UseVisualStyleBackColor = true;
            this.btnDetener2.Click += new System.EventHandler(this.btnDetener2_Click);
            // 
            // btnIniciar2
            // 
            this.btnIniciar2.Location = new System.Drawing.Point(45, 140);
            this.btnIniciar2.Name = "btnIniciar2";
            this.btnIniciar2.Size = new System.Drawing.Size(105, 23);
            this.btnIniciar2.TabIndex = 41;
            this.btnIniciar2.Text = "Iniciar";
            this.btnIniciar2.UseVisualStyleBackColor = true;
            this.btnIniciar2.Click += new System.EventHandler(this.btnIniciar2_Click);
            // 
            // frmAgregarCoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 222);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmAgregarCoto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificación de Firmas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnDetener;
        private System.Windows.Forms.Button bntIniciar;
        private Topaz.SigPlusNET sigPlusNET1;
        private System.Windows.Forms.Button btnGuardar2;
        private Topaz.SigPlusNET sigPlusNET2;
        private System.Windows.Forms.Button btnLimpiar2;
        private System.Windows.Forms.Button btnDetener2;
        private System.Windows.Forms.Button btnIniciar2;
    }
}