﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Registro
{
    public partial class frmAgregarCoto : Form
    {
        BD bd = new BD();
        int entra = 0;
        DataSet obtenDatos;

        public frmAgregarCoto()
        {
            InitializeComponent();

        }

        private void bntIniciar_Click(object sender, EventArgs e)
        {
            sigPlusNET1.SetTabletState(1);
        }

        private void btnDetener_Click(object sender, EventArgs e)
        {
            sigPlusNET1.SetTabletState(0);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            sigPlusNET1.ClearTablet();
        }

        private void btnIniciar2_Click(object sender, EventArgs e)
        {
            sigPlusNET2.SetTabletState(1);
        }

        private void btnDetener2_Click(object sender, EventArgs e)
        {
            sigPlusNET2.SetTabletState(0);
        }

        private void btnGuardar2_Click(object sender, EventArgs e)
        {

        }

        private void btnLimpiar2_Click(object sender, EventArgs e)
        {
            sigPlusNET2.ClearTablet();
        }

        
    }
}
