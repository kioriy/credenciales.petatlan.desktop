﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing.Imaging;

namespace Registro
{
    class CodeBar
    {
        //public string datos;
        //public Boolean parametros;
        //public int medida;

        //public CodeBar(string datos, Boolean parametros, int medida)
        //{
        //    this.datos = datos;
        //    this.parametros = parametros;
        //    this.medida = medida;
        //}

        public void Code128(string code, string path) 
        {
            Barcode128 code128 = new Barcode128();

            code128.CodeType = Barcode.CODE128;
            //code128.ChecksumText = true;
            //code128.GenerateChecksum = true;
            code128.Code = code;
            System.Drawing.Bitmap bm = new System.Drawing.Bitmap(code128.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
            bm.Save(path, System.Drawing.Imaging.ImageFormat.Gif); 

        }

        public void code39ext(string code, string path) 
        {
            Barcode39 code39ext = new Barcode39();


            code39ext.Code = code;
            code39ext.StartStopText = false;
            code39ext.Extended = true;

            System.Drawing.Bitmap bm = new System.Drawing.Bitmap(code39ext.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
            bm.Save(path + "\\code39ext.gif", System.Drawing.Imaging.ImageFormat.Gif);  
        }

        public void code39(string code, string path)
        {
            Barcode39 code39 = new Barcode39();

            code39.Code = code;
            code39.StartStopText = false;
            
            System.Drawing.Bitmap bm = new System.Drawing.Bitmap(code39.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
            bm.Save(path + "\\code39.gif", System.Drawing.Imaging.ImageFormat.Gif);
        }
    }
}
