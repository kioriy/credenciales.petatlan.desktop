﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registro
{
    class estados_consonantes
    {
        string est;
        public string n_estado(int x)
        {
            switch (x)
            {
                case 0: est = "AS"; break; //AGUASCALIENTES
                case 1: est = "BA"; break; //BAJA CALIFORNIA
                case 2: est = "BR"; break; //BAJA CLIFORNIA SUR
                case 3: est = "CE"; break; //CAMPECHE
                case 4: est = "CA"; break; //COAHUILA
                case 5: est = "CM"; break; //COLIMA
                case 6: est = "CS"; break;//CHIAPAS
                case 7: est = "CH"; break;// CHIHUAHUA
                case 8: est = "DF"; break;// D.F.
                case 9: est = "DO"; break;//DURANGO
                case 10: est = "GO"; break;//GUANAJUATO
                case 11: est = "GU"; break;//GUERRERO
                case 12: est = "HO"; break;//HIDALGO
                case 13: est = "JO"; break;//JALISCO
                case 14: est = "MX"; break;//MEXICO
                case 15: est = "MN"; break;//MICHOACÁN
                case 16: est = "MS"; break;//MORELOS
                case 17: est = "NT"; break;//NAYARIT
                case 18: est = "NL"; break;//NUEVO LEON
                case 19: est = "OA"; break;//OAXACA
                case 20: est = "PA"; break;//PUEBLA
                case 21: est = "QO"; break;//QUERETARO
                case 22: est = "QR"; break;//QUINTANA ROO
                case 23: est = "SL"; break;//SAN LUIS POTOSI
                case 24: est = "SA"; break;//SINALOA
                case 25: est = "SO"; break;//SONORA
                case 26: est = "TB"; break;//TABASCO
                case 27: est = "TS"; break;//TAMAULIPAS
                case 28: est = "TL"; break;//TLAXCALA
                case 29: est = "VZ"; break;//VERACRUZ
                case 30: est = "YN"; break;//YUCATAN
                case 31: est = "ZS"; break;//ZACATECAS
            }
            return est;
        }

        public string consontes(string apell_p, string apell_m, string nombre)
        {
            string consonantes="";

            for (int i = 1; i < apell_p.Length; i++)
            {
                if (apell_p.Substring(i, 1) == "B" || apell_p.Substring(i, 1) == "C" || apell_p.Substring(i, 1) == "D"
                    || apell_p.Substring(i, 1) == "F"|| apell_p.Substring(i, 1) == "G" || apell_p.Substring(i, 1) == "H"
                    || apell_p.Substring(i, 1) == "J" || apell_p.Substring(i, 1) == "K"|| apell_p.Substring(i, 1) == "L"
                    || apell_p.Substring(i, 1) == "M"|| apell_p.Substring(i, 1) == "N"|| apell_p.Substring(i, 1) == "Ñ"
                    || apell_p.Substring(i, 1) == "P" || apell_p.Substring(i, 1) == "Q" || apell_p.Substring(i, 1) == "R"
                    || apell_p.Substring(i, 1) == "S" || apell_p.Substring(i, 1) == "T" || apell_p.Substring(i, 1) == "V"
                    || apell_p.Substring(i, 1) == "W" || apell_p.Substring(i, 1) == "X"  || apell_p.Substring(i, 1) == "Y"
                    || apell_p.Substring(i, 1) == "Z")
                {
                    consonantes = apell_p.Substring(i, 1);
                    break;
                }
            }

            for (int j = 1; j < apell_m.Length; j++)
            {
                if (apell_m.Substring(j, 1) == "B" || apell_m.Substring(j, 1) == "C" || apell_m.Substring(j, 1) == "D"
                    || apell_m.Substring(j, 1) == "F" || apell_m.Substring(j, 1) == "G" || apell_m.Substring(j, 1) == "H"
                    || apell_m.Substring(j, 1) == "J" || apell_m.Substring(j, 1) == "K" || apell_m.Substring(j, 1) == "L"
                    || apell_m.Substring(j, 1) == "M" || apell_m.Substring(j, 1) == "N" || apell_m.Substring(j, 1) == "Ñ"
                    || apell_m.Substring(j, 1) == "P" || apell_m.Substring(j, 1) == "Q" || apell_m.Substring(j, 1) == "R"
                    || apell_m.Substring(j, 1) == "S" || apell_m.Substring(j, 1) == "T" || apell_m.Substring(j, 1) == "V"
                    || apell_m.Substring(j, 1) == "W" || apell_m.Substring(j, 1) == "X" || apell_m.Substring(j, 1) == "Y"
                    || apell_m.Substring(j, 1) == "Z")
                {
                    consonantes = consonantes + apell_m.Substring(j, 1);
                    break;
                }
            }

            for (int k = 1; k < nombre.Length; k++)
            {
                if (nombre.Substring(k, 1) == "B" || nombre.Substring(k, 1) == "C" || nombre.Substring(k, 1) == "D"
                    || nombre.Substring(k, 1) == "F" || nombre.Substring(k, 1) == "G" || nombre.Substring(k, 1) == "H"
                    || nombre.Substring(k, 1) == "J" || nombre.Substring(k, 1) == "K" || nombre.Substring(k, 1) == "L"
                    || nombre.Substring(k, 1) == "M" || nombre.Substring(k, 1) == "N" || nombre.Substring(k, 1) == "Ñ"
                    || nombre.Substring(k, 1) == "P" || nombre.Substring(k, 1) == "Q" || nombre.Substring(k, 1) == "R"
                    || nombre.Substring(k, 1) == "S" || nombre.Substring(k, 1) == "T" || nombre.Substring(k, 1) == "V"
                    || nombre.Substring(k, 1) == "W" || nombre.Substring(k, 1) == "X" || nombre.Substring(k, 1) == "Y"
                    || nombre.Substring(k, 1) == "Z")
                {
                    consonantes = consonantes + nombre.Substring(k, 1);
                    break;
                }
            }

            return consonantes;


        }

    }
}
