﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Configuration;
//using System.IO;

namespace Registro
{    
    class BD
    {
        string parametrosConexion = System.Configuration.ConfigurationManager.ConnectionStrings["db"].ConnectionString;
        SQLiteConnection conexionBD;
        SQLiteCommand queryCommand;
        public SQLiteDataAdapter adaptador;
        //SQLiteDataReader reader;
        //SQLiteParameter picture;
        //SQLiteParameter picture2;
        DataSet obtenerDatos;
        //string conectar = "server = localhost; database = credencializacion; UID = User; PWD = ''";
        //MySqlConnection conexionBD;
        //MySqlCommand RealizaQuery;
        //public MySqlDataAdapter adaptador;
        //DataSet obtenerDatos;

        
       public bool conexionAbrir()
       {
           conexionBD = new SQLiteConnection(parametrosConexion);
           ConnectionState estadoConexion;
           //conexionBD=new MySqlConnection(conectar);
           //ConnectionState estadoConexion;
           try
           {
              conexionBD.Open(); estadoConexion=conexionBD.State;
               return true;
           }
           catch (SQLiteException)
           {
           return false;
           }
       }

       private void conexionCerrar()
        {
           if (conexionBD.State == ConnectionState.Open)
           {
               conexionBD.Close();
           }
        }

       public bool ejecuta_comando(string query)
        {
            int filasAfectadas=0;
            conexionAbrir();

            queryCommand = new SQLiteCommand(query, conexionBD);
            //RealizaQuery = new MySqlCommand(query, conexionBD);
            filasAfectadas = queryCommand.ExecuteNonQuery(); 
           //filasAfectadas = RealizaQuery.ExecuteNonQuery();
            conexionCerrar();
            if (filasAfectadas > 0)
            {
                return true;
            }
            else {
                return false;
            }
        }

       public SQLiteDataAdapter consultaAdaptador(string RealizaConsulta)
       {
           conexionAbrir();
           adaptador = new SQLiteDataAdapter(RealizaConsulta, conexionBD);
           conexionCerrar();
           return adaptador;
       }

       public void llenarCombobox(System.Windows.Forms.ComboBox llenarComBox, string sqlLlenarComboBox, string columna, string tabla, ref int entra)
       {
          /* obtenerDatos = new DataSet();
           obtenerDatos.Reset();
           adaptador = consultaAdaptador(sqlLlenarComboBox);
           adaptador.Fill(obtenerDatos, tabla);
           entra=1;
           llenarComBox.DataSource = obtenerDatos.Tables[tabla];
           llenarComBox.DisplayMember = columna;
           llenarComBox.SelectedIndex = -1;
           entra = 0;*/
       }

       public void llenarListBox(System.Windows.Forms.ListBox llenarListBox, string sqlLlenarComboBox, string columna, string tabla, ref int entra)
       {
          /* obtenerDatos = new DataSet();
           obtenerDatos.Reset();
           adaptador = consultaAdaptador(sqlLlenarComboBox);
           adaptador.Fill(obtenerDatos, tabla);
           entra = 1;
           llenarListBox.DataSource = obtenerDatos.Tables[tabla];
           llenarListBox.DisplayMember = columna;
           llenarListBox.SelectedIndex = -1;
           entra = 0;*/
       }

    }
}
